package com.web;


import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class StudentHomePage
 */
@WebServlet("/StudentHomePage")
public class StudentHomePage extends HttpServlet {
	

protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
	response.setContentType("text/html");
	PrintWriter out = response.getWriter();
		
	out.print("<html>");
	out.print("<body bgcolor='lightyellow'>");
	out.print("<center>");
	out.print("<a href='StudentHomePage'>Home</a> &nbsp;");
	out.print("<a href='StudentLoginPage.html'>Logout</a>");
	out.print("<h1 style='color:green'>Welcome to Student Portal</h1>");	
	out.print("<center></body></html>");
		
}


	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
