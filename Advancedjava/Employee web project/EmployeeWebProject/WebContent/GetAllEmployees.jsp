
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" %>

<!-- Using the JSTL Core Tag -->
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>GetAllEmployees</title>
</head>
<body>
	<jsp:include page="HRHomePage.jsp" />
	<br />
	<table border='2' align='center'>
		<tr>
			<th>EmpId</th>
			<th>EmpName</th>
			<th>Salary</th>
			<th>Gender</th>
			<th>Email-Id</th>
			<th colspan='2'>Actions</th>
		</tr>
		<c:forEach var="employee" items="${employeeList}">		
		<tr>
			<td> ${ employee.empId   } </td>
			<td> ${ employee.empName } </td>
			<td> ${ employee.salary  } </td>
			<td> ${ employee.gender  } </td>
			<td> ${ employee.emailId } </td>
			<td> <a href='EditEmployee?empId=${employee.empId}'>  Edit  </a> </td>
			<td> <a href='DeleteEmployee?empId=${employee.empId}'>Delete</a> </td>
		</tr>		
		</c:forEach>
	</table>
</body>
</html>



