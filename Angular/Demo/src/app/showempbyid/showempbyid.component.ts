import { Component } from '@angular/core';
import { EmpService } from '../emp.service';

@Component({
  selector: 'app-showempbyid',
  templateUrl: './showempbyid.component.html',
  styleUrl: './showempbyid.component.css'
})
export class ShowempbyidComponent {

  emp: any;
  empId: number;
  employees: any;
  emailId: any;

  constructor(private service: EmpService) {
    this.empId = 0;

    //Fetching the EmailId from LocalStorage
    this.emailId = localStorage.getItem('emailId');
  }

  getEmployee() {
    this.emp = null;
    
    this.service.getEmployeeById(this.empId).subscribe((data: any) => {
      console.log(data);
      this.emp = data;
    });
  }
}

