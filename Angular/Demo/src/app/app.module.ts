import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TestComponent } from './test/test.component';
import { ShowemployeesComponent } from './showemployees/showemployees.component';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { ExpPipe } from './exp.pipe';
import { GenderPipe } from './gender.pipe';
import { FormsModule } from '@angular/forms';
import { ShowempbyidComponent } from './showempbyid/showempbyid.component';
import { ProductComponent } from './product/product.component';
import { LogoutComponent } from './logout/logout.component';
import { HeaderComponent } from './header/header.component';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { CartComponent } from './cart/cart.component';



@NgModule({
  declarations: [
    AppComponent,
    TestComponent,
    ShowemployeesComponent,
    RegisterComponent,
    LoginComponent,
    ExpPipe,
    GenderPipe,
    ShowempbyidComponent,
    ProductComponent,
    LogoutComponent,
    HeaderComponent,
    CartComponent,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    RouterModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
